# -*- ruby -*-

require 'uri'
require 'open-uri'
require 'net/http'
require 'date'
require 'set'
require 'nokogiri'
require 'digest/md5'
require 'rake/clean'
require 'tmpdir'
require 'pathname'

OSMOSIS			= ENV['OSMOSIS'] || 'osmosis'

PLANET_PBF_SOURCE	= 'https://planet.openstreetmap.org/pbf/'

PLANET_MD5_TEST 	= !true

SVN                     = ENV['SVN'] || 'svn'
DIR 			= { :planet	=> 'planet',
    			    :poly  	=> 'poly',
    			    :pbf	=> 'pbf',
    			    :map	=> 'map',
                            :xtra	=> 'xtra',
      			  }

GEOFABRIK_INDEX		= "http://download.geofabrik.de/"
OSMAND_POLY             = "https://github.com/osmandapp/OsmAnd-misc/trunk/osm-planet/polygons"

XTRA_PLANET_VERSION 	= "#{DIR[:xtra]}/planet-version"


PLANET_VERSION 		=  begin
                               File.open(XTRA_PLANET_VERSION).readline.strip
                           rescue Errno::ENOENT
                               '!!undefined!!'
                           end
PLANET_FILE		= "#{DIR[:planet]}/planet-#{PLANET_VERSION}.osm.pbf"

POLY 			= Rake::FileList["#{DIR[:poly]}/**/*.poly"]
if POLY.empty?
    warn "==> No polygon file (*.poly) found, try: rake poly:update"
end
def POLY.subregions
    psize = DIR[:poly].size
    Set.new(self.map {|p| p[psize+1..-1].sub(/\.poly$/, '') })
end
def POLY.lvl1
    self.subregions.inject({}) {|m, p|
        next if (s = p.split('/')).empty?
        (m[s[0]] ||= []) << s.join('/')
        m
    }
end


#==[ Commands ]========================================================

def mkdir_for(*files)
    files.map    {|f| File.dirname(f) }
         .reject {|d| Dir.exists?(d)  }
         .uniq
         .each   {|d| mkdir_p d       }
end

def osmcutter(src, cutter, meta: false)
    return if cutter.empty?
    args = []
    args << "--tee"      << cutter.size.to_s    if cutter.size > 1
    cutter.each {|poly, dst|
        args << "--bounding-polygon" << "file=#{poly}"
        args << "--write-pbf"        << "file=#{dst}"    
        args                         << "omitmetadata=true" if !meta
    }
    sh OSMOSIS, "--read-pbf", "file=#{src}", *args
rescue SystemExit, Interrupt, RuntimeError
    rm_f cutter.values
    raise
end

def mapconverter(pbf, map, threads: nil, memory: false)
    args = []
    args << "--read-pbf"         << "file=#{pbf}"
    args << "--mapfile-writer"   << "file=#{map}"
    args << "type=hd"            if !memory
    args << "threads=#{threads}" if threads&.> 0
    sh OSMOSIS, *args
rescue SystemExit, Interrupt, RuntimeError
    rm_f map
    raise
end

def fetch(url, file, empty: false, progress: true)
    url = URI(url) unless URI === url

    bsize = 0
    pbar  = %w[| / - \\]
    ibar  = 0
    sbar  = progress == true

    if Rake.verbose
        puts "fetch #{url}"     
        if sbar
            fname = File.basename(url.path)
            fname = fname[0,20] + '..' + fname[-20,20] if fname.size > 40
            print "Fetching #{fname}: "
        end
    end

    bytes  = 0
    length = nil
    ts     = nil
    open(file, 'w') {|o|
        Net::HTTP.start(url.host, url.port,
                        :use_ssl => url.scheme == 'https'
                       ) {|http|
            http.request_get(url.path) {|response|
                case response
                when Net::HTTPSuccess
                when Net::HTTPRedirection
                    fail "http redirect is not currently handled"
                else
                    fail "resource unavailable: #{url}"
                end
                ts     = (DateTime.parse response['last-modified'])
                    		.to_time rescue nil
                length = response.content_length
                length = nil if !length.nil? && length == 0
                response.read_body {|segment|
                    bytes += o.write(segment)
                    if Rake.verbose && sbar
                        if length
                            pct   = (100 * bytes) / length
                            str   = "%3d%% " % [ pct ]
                            print "\b"*bsize, str
                            bsize = str.length
                        end
                        ibar = (ibar + 1) % pbar.size
                        print "%c\b" % [ pbar[ibar] ]
                    end
                }
                
                print " \b", "\b"*bsize, "done\n" if Rake.verbose && sbar
            }
        }
    }
    File.utime(ts, ts, file) unless ts.nil?
    if bytes == 0 && empty == false
        fail "Fetched empty file"
    end
rescue RuntimeError
    print " \b", "\b"*bsize, "failed\n" if Rake.verbose && sbar
    File.unlink(file) rescue nil
    raise
rescue SystemExit, Interrupt
    File.unlink(file) rescue nil
    raise
end


def save_content(file, &block)
    File.open(file, 'w') {|o| block.call(o) }
rescue SystemExit, Interrupt, RuntimeError
    File.unlink(file) rescue nil
    raise
end


#==[ Functions ]=======================================================

def get_version_last_planet
    rev = Nokogiri::HTML(open(PLANET_PBF_SOURCE))
        .css('a[href^="planet-"][href$=".osm.pbf"]').map {|e|
        e[:href]=~ /^planet-(\d+).osm.pbf$/; $1
    }.compact.sort.last
end

def get_geofabrik_subregions
    world_url = URI(GEOFABRIK_INDEX)
    world     = Nokogiri::HTML(open(world_url))
    links     = []
    # Retrieve regions
    world.css('#details').remove
    world.css('#subregions .subregion a[href]').each {|l|
        region_url       = world_url.merge(l[:href])
        region_name      = l.content
        region           = Nokogiri::HTML(open(region_url))
        links << region_url.path[1..-1].sub(/\.html$/, '')
        # Retrieve subregions
        region.css('#details').remove
        region.css('#subregions        .subregion a[href],
                    #specialsubregions .subregion a[href]').each {|l|

            subregion_url      = region_url.merge(l[:href])
            unless subregion_url.path =~ %r{/north-america/us/}      ||
                   subregion_url.path =~ %r{/europe/alps\.}          ||
                   subregion_url.path =~ %r{/europe/british-isles\.} ||
                   subregion_url.path =~ %r{/europe/dach\.}

                links << subregion_url.path[1..-1].sub(/\.html$/, '')
            end
        }
    }
    links
end

def fetch_geofabrik_poly(sr, file)
    url    = URI(GEOFABRIK_INDEX).merge("#{sr}.poly")
    fetch url, file
end


def is_subregion_parent(psr, sr, include=false)
    _psr = psr.split('/')
    _sr  = sr.split('/').take(_psr.size)
    _psr == _sr && (include || psr != sr)
end

#==[ Tasks ]===========================================================

DIR.each_value {|dir|
    directory dir
}


desc "Fetch version of latest planet file."
task 'planet-latest' => [ DIR[:xtra] ] do 
    puts "Fetching version of latest planet" if Rake.verbose
    version = get_version_last_planet.to_s
    save_content(XTRA_PLANET_VERSION) {|io| io.puts version }
    warn "==> Stopping here as planet version as been updated (#{version})"
    exit
end


desc "Set the version of planet file to use."
task 'planet-setup', [ :version ] => [ DIR[:xtra] ] do |t,args|
    version = args.version
    save_content(XTRA_PLANET_VERSION) {|io| io.puts version }
    warn "==> Stopping here as planet version as been updated (#{version})"
    exit
end


task 'planet-version' do 
    if PLANET_VERSION.nil? || PLANET_VERSION !~ /^\d+$/
        warn "No planet version as been defined!"
        warn "==> I'm running for you: rake planet-latest"
        Rake::Task['planet-latest'].invoke
    else
        puts "Using planet version: #{PLANET_VERSION}"
    end
end


desc "List available sub-regions"
task :list do
    puts POLY.subregions.to_a
end



task "#{PLANET_FILE}.md5" => [ 'planet-version' ] do |t|
    next if File.exists?(t.name)
    url = URI(PLANET_PBF_SOURCE).merge("planet-#{PLANET_VERSION}.osm.pbf.md5")
    mkdir_for t.name
    fetch url, t.name
end

task PLANET_FILE => [ 'planet-version' ] do |t|
    md5  = if PLANET_MD5_TEST
               Rake::Task["#{PLANET_FILE}.md5"].invoke
               File.open("#{PLANET_FILE}.md5").readline.split(' ').first
           end
    load = if    !File.exists?(t.name)
               true
           elsif md5 && Digest::MD5.file(t.name).hexdigest != md5
               warn "Checksum failed (#{t.name}) (reloading)"
               true
           end
    if load
        url = URI(PLANET_PBF_SOURCE).merge("planet-#{PLANET_VERSION}.osm.pbf")
        mkdir_for t.name
        fetch url, t.name
        if md5 && Digest::MD5.file(t.name).hexdigest != md5
            fail "Checksum mismatch #{t.name}"
        end
        time = case PLANET_VERSION
               when /^(\d{2})(\d{2})(\d{2})$/ 
                   Time.new("20#{$1}".to_i, $2.to_i, $3.to_i)
               end
        File.utime(time, time, t.name) unless time.nil?
    end
end




desc "Remove all defined subregions."
task 'pbf-' do
    rm_rf DIR[:pbf]
end



desc "Build all defines subregions."
task 'pbf+' => [ 'pbf+1' ]  do
    POLY.lvl1.each {|k,v| Rake::Task["pbf/#{k}~" ].invoke }
end


task 'pbf+1' => [ PLANET_FILE ] do
    lvl1   = POLY.lvl1.reject {|k,v| !v.include?(k) }.keys
    cutter = Hash[lvl1.map {|l| 
                      poly = "#{DIR[:poly]}/#{l}.poly"
                      pbf  = "#{DIR[:pbf ]}/#{l}.osm.pbf"
                      [poly, pbf] unless uptodate?(pbf, [PLANET_FILE, poly]) 
                  }.compact]
    unless cutter.empty?
        mkdir_for *cutter.values
        osmcutter(PLANET_FILE, cutter)
    end
end

rule(/^pbf\/.+[\-\+\~]?/ => [ PLANET_FILE ]) do |t|
    tn   = t.name.sub(/([\-\+\~])$/, '')
    op   = $1
    sr   = tn.split('/')[1..-1].join('/')


    prep = lambda {|sr|
        poly = "#{DIR[:poly]}/#{sr}.poly"
        pbf  = "#{DIR[:pbf ]}/#{sr}.osm.pbf"
        psr  = sr.split('/')
        src  = PLANET_FILE
        while !psr.pop.nil?
            ppbf = "#{DIR[:pbf]}/#{psr.join('/')}.osm.pbf"
            src  = ppbf if File.exists?(ppbf)
        end
        if !uptodate?(pbf, [ src, poly ]) 
        then [src, poly, pbf ]
        end
    }

    case op
    when nil
        src, poly, pbf = prep.call(sr)
        if src
            mkdir_for pbf
            osmcutter(src, poly => pbf)
        end
    when '+'
        Rake::Task["pbf/#{sr}" ].invoke
        Rake::Task["pbf/#{sr}~"].invoke
    when '~'
        h = {}
        POLY.subregions
            .select {|_sr| is_subregion_parent(sr, _sr)    }
            .each   {|_sr| src, poly, pbf = prep.call(_sr)
                           next unless src 
                           mkdir_for pbf
                           (h[src] ||= {})[poly] = pbf     }
        h.each{|src, cut|
            osmcutter(src, cut)
        }
    when '-'
        rm_f  "#{DIR[:pbf]}/#{sr}.osm.pbf"
        rm_rf "#{DIR[:pbf]}/#{sr}"
    end
end



desc "Remove all maps."
task 'map-' do
    rm_rf DIR[:map]
end

desc "Build all maps."
task 'map+' => [ 'pbf+' ] do
    POLY.subregions.each {|sr| Rake::Task["map/#{sr}"].invoke }
end

desc "Build all leaf maps."
task 'map~' => [ 'pbf+' ] do
    s = POLY.subregions
    s.select {|sr| s.include?(sr.split('/')[0..-2].join('/')) }
     .each   {|sr| Rake::Task["map/#{sr}"].invoke } 
end

rule(/^map\/.+[\-\+\~]?/ => [
            proc { |tn| 'pbf/' + tn.split('/')[1..-1].join('/') } 
        ]) do |t|
    tn   = t.name.sub(/([\-\+\~])$/, '')
    op   = $1
    sr   = tn.split('/')[1..-1].join('/')

    case op
    when nil
        map  = "#{DIR[:map ]}/#{sr}.map"
        pbf  = "#{DIR[:pbf ]}/#{sr}.osm.pbf"
        if !uptodate?(map, [ pbf ])
            mkdir_for map
            mapconverter(pbf, map)
        end
    when '+'
        Rake::Task["map/#{sr}" ].invoke
        Rake::Task["map/#{sr}~"].invoke
    when '~'
        POLY.subregions.each {|_sr|
            next unless is_subregion_parent(sr, _sr)
            Rake::Task["map/#{_sr}"].invoke
        }
    when '-'
        rm_f  "#{DIR[:map]}/#{sr}.map"
        rm_rf "#{DIR[:map]}/#{sr}"
    end
end


task :default => [ PLANET_FILE ]

desc "Update the poly files (from: GeoFabrik, OsmAnd)"
task :'poly:update' do
    copy_to_poly = lambda {|src, dstinfo, orig: nil, poly: nil|
        dst, globs = src, [ '*.poly' ]
        case dstinfo
        when Symbol then dst   = dstinfo.to_s
        when String then globs = [ dstinfo ]
        when Array
            dst   = dstinfo.shift if Symbol === dstinfo.first
            globs = dstinfo
        end
        mkdir_p "#{poly}/#{dst}"
        puts "#{orig}/#{src}.poly => #{poly}"
        cp "#{orig}/#{src}.poly", "#{poly}"
        globs.flat_map {|glob| Dir["#{orig}/#{src}/#{glob}"] }
            .each {|file| cp file, "#{poly}/#{dst}" }
    }
    
    
    Dir.mktmpdir {|dir|
        osmand = "#{dir}/poly.osmand"
        geo    = "#{dir}/poly.geo"
        poly   = "#{dir}/poly"

        mkdir_p poly

        # Retrieve poly from OsmAnd
        sh SVN, 'export', OSMAND_POLY, osmand
        rm "#{osmand}/australia-oceania/australia-oceania.poly" 
        mv "#{osmand}/europe/west-europe/gb/northern_ireland.poly",
           "#{osmand}/europe/west-europe/gb/northern-ireland.poly"
        mv "#{osmand}/australia-oceania-all.poly",
           "#{osmand}/australia-oceania.poly" 
        { 'africa'            => nil,
          'europe'            => [ '*/*.poly', '*/gb/*.poly' ],
          'asia'              => [ '*.poly', '*-asia/*.poly' ],
          'australia-oceania' => nil,
          'central-america'   => nil,
          'north-america'     => nil,
          'south-america'     => nil }.each {|src, dstinfo|
            copy_to_poly.call(src, dstinfo, orig: osmand, poly: poly)
        }
        { 'france'        => 'europe',
          'germany'       => 'europe',
        # 'great-britain' => 'europe',
          'russia'        => nil,
          'antarctica'    => nil,
          'us'            => 'north-america'
        }.each {|country,dest|
            cp "#{osmand}/#{country}.poly", "#{poly}/#{dest}"
        }
        
        # Retrieve poly from GeoFabrik
        get_geofabrik_subregions.each {|sr| sr.strip!
            file = "#{dir}/poly.geo/#{sr}.poly"
            mkdir_for  file
            fetch_geofabrik_poly sr, file
        }
        rm "#{geo}/africa/south-africa-and-lesotho.poly"
        rm "#{geo}/africa/senegal-and-gambia.poly"
        rm "#{geo}/asia/gcc-states.poly"
        rm "#{geo}/asia/israel-and-palestine.poly"
        rm "#{geo}/asia/malaysia-singapore-brunei.poly"        
        { 'africa'            => nil,
          'europe'            => nil,
          'asia'              => nil,
          'australia-oceania' => nil,
          'central-america'   => nil,
          'north-america'     => nil,
          'south-america'     => nil }.each {|src, dstinfo|
            copy_to_poly.call(src, dstinfo, orig: geo, poly: poly)
        }
        { 'russia'        => nil,
          'antarctica'    => nil,
        }.each {|country,dest|
            cp "#{geo}/#{country}.poly", "#{poly}/#{dest}"
        }

        # For now prefer the unified version of geofabrik
        rm "#{poly}/europe/northern-ireland.poly"
        rm "#{poly}/europe/ireland.poly"
        # Great britain has been split into individual countries
        rm "#{poly}/europe/great-britain.poly"

        # Merge with local poly directory
        Hash[Dir["#{poly}/*.poly", "#{poly}/**/*.poly"].map {|file|
                 relfile = Pathname(file).relative_path_from(Pathname(poly))
                 [ file, "#{DIR[:poly]}/#{relfile}"]
             }].tap {|h      |
                 mkdir_for *h.values 
             }.each {|src,dst| 
                 unless File.exists?(dst) &&
                        Digest::MD5.file(src) == Digest::MD5.file(dst)
                     cp src, dst
                 end
             }
    }
end
