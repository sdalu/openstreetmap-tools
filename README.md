openstreetmap-tools
===================

Requirement
-----------
* ruby
* rake and nokogiri (or use the `Gemfile` with [bundler](http://bundler.io/))
* [osmosis](https://wiki.openstreetmap.org/wiki/Osmosis)
* [mapsforge-map-writer-0.9.1-jar-with-dependencies.jar](http://repo1.maven.org/maven2/org/mapsforge/mapsforge-map-writer/0.9.1/mapsforge-map-writer-0.9.1-jar-with-dependencies.jar)

Configuration
-------------
Copy the mapsforge plugin into the openstreetmap plugin directory: 
```sh
mkdir -p ${HOME}/.openstreetmap/osmosis/plugins/
cp mapsforge-map-writer-0.9.1-jar-with-dependencies.jar ${HOME}/.openstreetmap/osmosis/plugins/
```

If needed define the ``OSMOSIS`` environment variable or the ``osmosis`` programme will be 
searched in the current ``$PATH``
```sh
export OSMOSIS=/path/to/bin/osmosis
```

You absolutely need to configure Java for Osmosis.
The configuration file is ``${HOME}/.osmosis`` and the variable is ``JAVACMD_OPTIONS``

| Option                    | Meaning                                 | Possible value |
| :------------------------ |:--------------------------------------- | --------------:|
| `-server`                 | Optimized for performance               |                |
| `-d64`                    | Use a 64-bit data model if available    |                |
| `-Xms${size_start}`       | Memory allocated at startup             |            10G |
| `-Xmx${size_max}`         | Maximum memory allocated                |            40G |
| `-Djava.io.tmpdir=${dir}` | Directory where to store temporary data |           /tmp |
| `-XX:-UseGCOverheadLimit` | Deal with "GC overhead limit exceeded"  |                |
| `-XX:+UseParallelGC`      | Use the parallel scavenge collector     |                |

This would result in the following configuration file:
```
JAVACMD_OPTIONS="-server -Xms10G -Xmx40G -d64 -Djava.io.tmpdir=/tmp -XX:-UseGCOverheadLimit -XX:+UseParallelGC"
```

Note that you need to ensure that:
* you have enough memory 
* you have enough disk space for the temporary data directory, and it is preferrably located on an SSD disk

Start
-----
```sh
rake       # Will retrieve version of the latest planet and stop (first run)
rake       # Will download latest planet file from openstreetmap
rake pbf+  # Will generate all the subregions from planet file
rake map~  # Will create all the mapsforge maps (excluding parent maps)
rake -T    # Will give you more informations
```

Polygons
--------
Polygon files for country boundaries are a mix from [GeoFabrik](http://download.geofabrik.de/) and [OsmAnd](https://github.com/osmandapp/OsmAnd-misc/tree/master/osm-planet/polygons)
